
package com.yahoo.ycsb.db;

import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

import com.yahoo.ycsb.ByteIterator;
import com.yahoo.ycsb.DBException;
import com.yahoo.ycsb.Status;
import com.yahoo.ycsb.StringByteIterator;

import kinetic.client.Entry;
import kinetic.client.KineticException;
import kinetic.client.advanced.AdvancedKineticClient;
import kinetic.client.advanced.PersistOption;
import kinetic.client.ClientConfiguration;
import kinetic.client.advanced.AdvancedKineticClientFactory;

import kinetic.admin.AdminClientConfiguration;
import kinetic.admin.KineticAdminClient;
import kinetic.admin.KineticAdminClientFactory;

import java.nio.charset.Charset;


/**
 * Rest client for YCSB framework for the restful server
 */
public class Kinetic extends com.yahoo.ycsb.DB {

//  String target = "10.3.1.2";
//  String target = "192.168.0.136";
//    String target = "10.3.1.3";
    String target = "127.0.0.1";
    int port = 8443;
    boolean useSSL = true;
    AdvancedKineticClient client = null;
    private static KineticAdminClient kineticAdminClient = null;


    private void eraseDrive() throws DBException{
        AdminClientConfiguration adminClientConfig = new AdminClientConfiguration();
        adminClientConfig.setHost(target);
        adminClientConfig.setUseSsl(true);
        adminClientConfig.setPort(8443);
        System.out.println("Erasing drive...");

        try {
            kineticAdminClient = KineticAdminClientFactory.createInstance(adminClientConfig);
            String erasePin = "";
            byte[] erasePinB = erasePin.getBytes(Charset.forName("UTF-8"));
            kineticAdminClient.instantErase(erasePinB);
            kineticAdminClient.close();
        } catch (KineticException e) {
            System.out.println("Failed to erase drive " + target + ":"  + port + ", exit the branchmark.");
            throw new DBException(e.getMessage());
        }
    }

    private void setup() throws DBException{
	    target = getProperties().getProperty("kinetic_target", target);
        useSSL = Boolean.parseBoolean(getProperties().getProperty("kinetic_useSSL", String.valueOf(useSSL)));
        port = Integer.parseInt(getProperties().getProperty("kinetic_port", String.valueOf(port)));

        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setUseSsl(useSSL);
        clientConfig.setHost(target);
        clientConfig.setPort(port);

        try {
            client = AdvancedKineticClientFactory.createAdvancedClientInstance(clientConfig);
        } catch (KineticException e) {
            System.out.println("Failed to connect node " + target + ":"  + port + ", exit the branchmark.");
            throw new DBException(e.getMessage());
        }
    }

    /**
     * Initialize any state for this DB. Called once per DB instance; there is
     * one DB instance per client thread.
     */

    @Override
    public void init() throws DBException {
    	setup();
    }

    /**
     * Cleanup any state for this DB. Called once per DB instance; there is one
     * DB instance per client thread.
     */
    @Override
    public void cleanup() throws DBException {
        try {
            client.close();
        } catch (KineticException e) {
            throw new DBException(e.getMessage());
        }
    }

    /**
     * Read a record from the database. Each field/value pair from the result
     * will be stored in a HashMap.
     *
     * @param table  The name of the table
     * @param key    The record key of the record to read.
     * @param fields The list of fields to read, or null for all of them
     * @param result A HashMap of field/value pairs for the result
     * @return The result of the operation.
     */
    @Override
    public Status read(String table, String key, Set<String> fields, HashMap<String, ByteIterator> result) {

        Entry entry = null;
        byte[] keyAsBytes = key.getBytes();

        try {
            entry = client.get(keyAsBytes);
        } catch (KineticException e) {
            System.out.println(e.getResponseMessage());
	    try {
		    cleanup();
		    setup();
            } catch (Exception ee) {

        }
    //        return Status.ERROR;
        }

        // does not support fields
        if (entry == null)
            return Status.NOT_FOUND;
        else {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put(key, new String(entry.getValue()));
            StringByteIterator.putAllAsByteIterators(result, map);
        }

        return Status.OK;
    }

    /**
     * Perform a range scan for a set of records in the database. Each
     * field/value pair from the result will be stored in a HashMap.
     *
     * @param table       The name of the table
     * @param startkey    The record key of the first record to read.
     * @param recordcount The number of records to read
     * @param fields      The list of fields to read, or null for all of them
     * @param result      A Vector of HashMaps, where each HashMap is a set field/value
     *                    pairs for one record
     * @return The result of the operation.
     */
    @Override
    public Status scan(String table, String startkey, int recordcount, Set<String> fields,
                       Vector<HashMap<String, ByteIterator>> result) {
        return Status.NOT_IMPLEMENTED;
    }

    /**
     * Update a record in the database. Any field/value pairs in the specified
     * values HashMap will be written into the record with the specified record
     * key, overwriting any existing values with the same field name.
     *
     * @param table  The name of the table
     * @param key    The record key of the record to write.
     * @param values A HashMap of field/value pairs to update in the record
     * @return The result of the operation.
     */
    @Override
    public Status update(String table, String key, HashMap<String, ByteIterator> values) {
        return this.insert(table, key, values);
    }

    /**
     * Insert a record in the database. Any field/value pairs in the specified
     * values HashMap will be written into the record with the specified record
     * key.
     *
     * @param table  The name of the table
     * @param key    The record key of the record to insert.
     * @param values A HashMap of field/value pairs to insert in the record
     * @return The result of the operation.
     */
    @Override
    public Status insert(String table, String key, HashMap<String, ByteIterator> values) {
        Entry entry = new Entry();
        entry.setKey(key.getBytes());
        if (!values.isEmpty()) {
//		byte[] t = values.values().iterator().next().toArray();
//	        entry.setValue(t);
//		System.out.println(new String(t));
		entry.setValue(values.values().iterator().next().toArray());
        }
        try {
            client.put(entry, null, PersistOption.ASYNC);
        } catch (KineticException e) {
	    try {
                    cleanup();
                    setup();
            } catch (Exception ee) {}

        //    return Status.ERROR;
        }
        return Status.OK;
    }

    /**
     * Delete a record from the database.
     *
     * @param table The name of the table
     * @param key   The record key of the record to delete.
     * @return The result of the operation.
     */
    @Override
    public Status delete(String table, String key) {
        Entry entry = new Entry();
        entry.setKey(key.getBytes());
        try {
            if (client.delete(entry))
                return Status.OK;
            else
                return Status.ERROR;
        } catch (KineticException e) {
            return Status.ERROR;
        }

    }

}
