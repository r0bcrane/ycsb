#!/bin/bash
# Please change this according to your ycsb installation
# so that, ${YCSB_HOME}/bin/ycsb is the binary 
YCSB_HOME=.

for setting in `ls ycsb_workload_settings`
do
    FILE=${setting%.dat}
    if [ ! -f raw_workloads/${FILE}_a.load ] ; then
	    echo generateing $setting.load, the insertions used before benchmark
	    ${YCSB_HOME}/bin/ycsb load basic -P workloads/workloada -P ycsb_workload_settings/$setting > raw_workloads/${FILE}_a.load
	    echo generateing $setting.run, the lookup queries used before benchmark
	    ${YCSB_HOME}/bin/ycsb run basic -P workloads/workloada -P ycsb_workload_settings/$setting > raw_workloads/${FILE}_a.run
    fi
done

for setting in `ls ycsb_workload_settings`
do
    FILE=${setting%.dat}
    if [ ! -f raw_workloads/${FILE}_b.load ] ; then
	    echo generateing $setting.load, the insertions used before benchmark
	    ${YCSB_HOME}/bin/ycsb load basic -P workloads/workloadb -P ycsb_workload_settings/$setting > raw_workloads/${FILE}_b.load
	    echo generateing $setting.run, the lookup queries used before benchmark
	    ${YCSB_HOME}/bin/ycsb run basic -P workloads/workloadb -P ycsb_workload_settings/$setting > raw_workloads/${FILE}_b.run
    fi
done

for setting in `ls ycsb_workload_settings`
do
    FILE=${setting%.dat}
    if [ ! -f raw_workloads/${FILE}_c.load ] ; then
	    echo generateing $setting.load, the insertions used before benchmark
	    ${YCSB_HOME}/bin/ycsb load basic -P workloads/workloadc -P ycsb_workload_settings/$setting > raw_workloads/${FILE}_c.load
	    echo generateing $setting.run, the lookup queries used before benchmark
	    ${YCSB_HOME}/bin/ycsb run basic -P workloads/workloadc -P ycsb_workload_settings/$setting > raw_workloads/${FILE}_c.run
    fi
done

for setting in `ls ycsb_workload_settings`
do
    FILE=${setting%.dat}
    if [ ! -f raw_workloads/${FILE}_d.load ] ; then
	    echo generateing $setting.load, the insertions used before benchmark
	    ${YCSB_HOME}/bin/ycsb load basic -P workloads/workloadd -P ycsb_workload_settings/$setting > raw_workloads/${FILE}_d.load
	    echo generateing $setting.run, the lookup queries used before benchmark
	    ${YCSB_HOME}/bin/ycsb run basic -P workloads/workloadd -P ycsb_workload_settings/$setting > raw_workloads/${FILE}_d.run
    fi
done

for setting in `ls ycsb_workload_settings`
do
    FILE=${setting%.dat}
    if [ ! -f raw_workloads/${FILE}_e.load ] ; then
	    echo generateing $setting.load, the insertions used before benchmark
	    ${YCSB_HOME}/bin/ycsb load basic -P workloads/workloade -P ycsb_workload_settings/$setting > raw_workloads/${FILE}_e.load
	    echo generateing $setting.run, the lookup queries used before benchmark
	    ${YCSB_HOME}/bin/ycsb run basic -P workloads/workloade -P ycsb_workload_settings/$setting > raw_workloads/${FILE}_e.run
    fi
done
